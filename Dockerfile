FROM yiisoftware/yii2-php:7.1-apache
WORKDIR /app
COPY . /app

# Setup Apache Doc Root
# ENV APACHE_DOCUMENT_ROOT /var/www/web/
# RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
# RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*
# RUN echo "date.timezone = Asia/Manila" > $PHP_INI_DIR/conf.d/timezone.ini

# Install System Prerequisites
# RUN apt update && apt install -y \
# 	git \
# 	unzip \
# 	libxml2-dev \
# 	unixodbc-dev \
# 	libssl-dev \
# 	libcurl4-openssl-dev \
# 	pkg-config \
# 	apt-transport-https \
# 	gnupg \
# 	unixodbc-dev \
# 	wkhtmltopdf \
# 	xvfb \
# 	g++ \
# 	zlib1g-dev \
# 	libicu-dev

# Download Composer
# RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
# RUN php composer-setup.php --install-dir=/usr/local/bin
# RUN php -r "unlink('composer-setup.php');"

# Install PHP extensions
# RUN docker-php-ext-install ctype curl iconv intl posix mbstring opcache pdo pdo_mysql tokenizer xml
# RUN docker-php-ext-enable ctype curl iconv intl posix mbstring opcache pdo pdo_mysql tokenizer xml

# Install App Dependencies
# RUN composer.phar install
RUN chown -R www-data:www-data .

EXPOSE 80
